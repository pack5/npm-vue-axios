## Installing

Using npm:

```bash
$ npm i vue-ldc-axios
```

## Example

```js
import { apiGet } from 'vue-ldc-axios/api';

apiGet('domain', {
    params: params
}).then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
})
```

### Environment Variables
Name | Type | Default value | Description
:---: | :---: | :---: | :---: |
*VUE_APP_API_URL* | String | / | Base domain api.
*VUE_APP_KEY_ACCESS_TOKEN* | String | access_token | Key get token in cookie.
*VUE_APP_KEY_REFRESH_TOKEN* | String | Null | Key get refresh token in cookie.
*VUE_APP_TOKEN_BEARER* | String | Null | Bearer before token.
*VUE_APP_KEY_SET_AUTH* | String | Authorization | Key set header authentication.

## Note Environment Variables
VUE_APP_KEY_REFRESH_TOKEN but null there will be no mechanism refresh token

when VUE_APP_KEY_REFRESH_TOKEN yes then we will get function **callRefreshToken** from *'@/config/vue-ldc-axios'* handle the refresh token

Note that the **callRefreshToken** function must be a Promise, if not a Promise, we are not responsible for the results returned to you.

If the token refresh is successful, the Promise return the token, not the refresh token

**callRefreshToken** you will save your own token and refresh token


**Methods:**
```js
import 'vue-ldc-axios';
import { apiGet, apiPost, apiPut, apiDelete } from 'vue-ldc-axios/api';

# api get
apiGet('domain', {
    params: params
}).then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
})

# api post
apiGet('domain', {
    form: form
}, {
    // headers
}).then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
})

# api put
apiPut('domain', {
    form: form
}, {
    // headers
}).then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
})

# api delete
apiDelete('domain', {
    params: params
}).then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
})
```

## Note method
All catch we run through a function **actionCatchApi** from *'@/config/vue-ldc-axios'* for you to handle in general


## Git
[nglinhbk](https://gitlab.com/pack5/npm-vue-axios/-/tree/main/npm)
