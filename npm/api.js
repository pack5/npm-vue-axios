import {
    actionCatchApi,
    // notOnline, notResponse,
    // unauthorized, serverError,
    // babRequest, unprocessableEntity,
    // forbidden,
} from '@/config/vue-ldc-axios';

export const isOnline = () => {
    return true;
    let online = window.navigator.onLine;
    if(!online) {
        if(typeof notOnline == 'function') {
            notOnline();
        }
    }
    return online;
}

// export const actionCatchApi = (error) => {
//     if(!error.response){
//         if(typeof notResponse == 'function'){
//             notResponse();
//         }
//     }else if(error.response.status === HTTP_CODE_UNAUTHORIZED){
//         if(typeof unauthorized == 'function'){
//             unauthorized();
//         }
//     }else if(error.response.status === HTTP_CODE_SERVER_ERROR){
//         if(typeof serverError == 'function'){
//             serverError();
//         }
//     }else if(error.response.status === HTTP_CODE_BAD_REQUEST){
//         if(typeof babRequest == 'function'){
//             babRequest();
//         }
//     }else if(error.response.status === HTTP_CODE_UNPROCESSABLE_ENTITY){
//         if(typeof unprocessableEntity == 'function'){
//             unprocessableEntity();
//         }
//     }else if(error.response.status === HTTP_CODE_FORBIDDEN){
//         if(typeof forbidden == 'function'){
//             forbidden();
//         }
//     }
// }

export const catchApi = async (error) => {
    if(typeof actionCatchApi == 'function'){
        await actionCatchApi(error);
    }
}

export const returnError = (error) => {
    return {
        data: error.response.data,
        status: error.response.status,
    }
}

export const apiGet = async (url, data = {}) => {
    if(!isOnline()){
        return;
    }
    return await axios.get(url, {
        params : data
    }).then(async response => {
        return response.data;
    }).catch(async error => {
        await catchApi(error);
        throw returnError(error);
    })
}

export const apiPost = async (url, data, params = {}) => {
    if(!isOnline()){
        return;
    }
    return await axios.post(url, data, {
        params: params
    }).then(async res => {
        return res.data;
    }).catch(async error => {
        await catchApi(error);
        throw returnError(error);
    });
}

export const apiPut = async (url, data, params = {}) => {
    if(!isOnline()){
        return;
    }
    return await axios.put(url, data, {
        params: params
    }).then(async res => {
        return res.data;
    }).catch(async error => {
        await catchApi(error);
        throw returnError(error);
    });
}

export const apiDelete = async (url, data = {}) => {
    if(!isOnline()){
        return;
    }
    return axios.delete(url, data).then(async res => {
        return res;
    }).catch(async error => {
        await catchApi(error);
        throw returnError(error);
    });
}
