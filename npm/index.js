import { getToken, setAuthorization, setConfigAuthorization } from './token';

if(typeof HTTP_CODE_UNAUTHORIZED == 'undefined') {
    window.HTTP_CODE_UNAUTHORIZED = 401;
}
// if(typeof HTTP_CODE_SERVER_ERROR == 'undefined') {
//     window.HTTP_CODE_SERVER_ERROR = 500;
// }
// if(typeof HTTP_CODE_BAD_REQUEST == 'undefined') {
//     window.HTTP_CODE_BAD_REQUEST = 400;
// }
// if(typeof HTTP_CODE_UNPROCESSABLE_ENTITY == 'undefined') {
//     window.HTTP_CODE_UNPROCESSABLE_ENTITY = 422;
// }
// if(typeof HTTP_CODE_FORBIDDEN == 'undefined') {
//     window.HTTP_CODE_FORBIDDEN = 403;
// }

window.axios = require('axios');
axios.defaults.baseURL = process.env.VUE_APP_API_URL;

const key_access_token = process.env.VUE_APP_KEY_ACCESS_TOKEN ? process.env.VUE_APP_KEY_ACCESS_TOKEN : 'access_token';

let tokenLocal = getToken(key_access_token);

if(tokenLocal) {
    setAuthorization(tokenLocal);
}

import { callRefreshToken } from '@/config/vue-ldc-axios';

const key_refresh_token = process.env.VUE_APP_KEY_REFRESH_TOKEN;
if(key_refresh_token && typeof callRefreshToken == 'function') {
    
    axios.interceptors.response.use(async res => {
        return res;
    }, async err => {
        if(err.response.status === HTTP_CODE_UNAUTHORIZED) {
            const refreshToken = getToken(key_refresh_token);
            if(!refreshToken) {
                return Promise.reject(err);
            }
            return await callRefreshToken(refreshToken).then(async resRef => {
                setAuthorization(resRef);
                setConfigAuthorization(resRef, config);
                return await axios(err.response.config)
            }).catch(error => {
                return Promise.reject(err);
            })
        }else{
            return Promise.reject(err);
        }
    });
}

