export const saveToken = (nameToken, data) => {
    let exp = data.expire * 1000;
    let time = new Date(exp);
    let token = data.token;
    document.cookie = nameToken + '=' + token +'; expires=' + time + '; path=/;';
}

export const getToken = (nameToken) => {
    let dataToken = document.cookie.split(';');
    nameToken += '=';
    let token = '';
    for(var i = 0; i <dataToken.length; i++) {
        if(dataToken[i].includes(nameToken) == true){
            token = dataToken[i];
        }
    }
    if(token != ''){
        token = token.split('=')[1];
        return token;
    }else{
        return '';
    }
}

export const setAuthorization = (token) => {
    axios.defaults.headers.common[getKeyAuth()] = setToken(token);
}
export const setConfigAuthorization = (token, config) => {
    config.headers.common[getKeyAuth()] = setToken(token);
}

export const getKeyAuth = () => {
    const key_set_auth = process.env.VUE_APP_KEY_SET_AUTH ? process.env.VUE_APP_KEY_SET_AUTH : 'Authorization';
    return key_set_auth;
}

export const setToken = (token) => {
    let bearer = process.env.VUE_APP_TOKEN_BEARER;
    if(bearer) {
        bearer + ' ';
    }else {
        bearer = '';
    }

    return bearer + token;
}